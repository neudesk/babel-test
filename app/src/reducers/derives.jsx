export default function reducer(state = { email: null }, action) {
    switch (action.type) {
        case 'STORE_DERIVED_EMAIL': {
            return {...state, email: action.payload}
        }
        default: {
            return state;
        }
    }
    return state
}