import { combineReducers } from 'redux';
import Derives from './derives'

const CombinedReducers = combineReducers({ Derives });

export default CombinedReducers;