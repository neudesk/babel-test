import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import NotificationSystem from 'react-notification-system';

export default class Dashboard extends Component {

    _notificationSystem = null;

    constructor(props) {
        super(props)
        this.state = {
            first_name: '',
            last_name: '',
            domain: ''
        }
    }

    componentDidMount() {
        this._notificationSystem = this.refs.notificationSystem;
    }

    onFieldChange(e, field) {
        const self = this;
        const current_state = self.state;
        current_state[field] = e.target.value
        self.setState(current_state);
    }

    deriveEmail() {
        const self = this;
        const app = self.props.app;
        const actions = app.actions.derive_email_actions
        actions.derivedEmail(self.state, self._notificationSystem)
    }

    render() {
        const self = this;
        const app = self.props.app;
        let derived_email = null;

        if (app.derived_email) {
            derived_email = <div className="row">
                <div className="col s12 text-center">
                    Your email is: { app.derived_email }
                </div>
            </div>
        }

        return(
            <div className="container">
                <div className="row">
                    <div className="col s12" style={ { marginTop: '20%' } }>
                        <h4 className="text-center">Please fill up form below to derive your email.</h4>
                        <h5>{derived_email}</h5>
                    </div>
                </div>
                <div className="row">
                    <div className="col s4">
                        <TextField className="full-width" floatingLabelText="First Name"
                                   onChange={ (e) => { self.onFieldChange(e, 'first_name') } } />
                    </div>
                    <div className="col s4">
                        <TextField className="full-width" floatingLabelText="Last Name"
                                   onChange={ (e) => { self.onFieldChange(e, 'last_name') } } />
                    </div>
                    <div className="col s4">
                        <TextField className="full-width" floatingLabelText="Domain Name"
                                   onChange={ (e) => { self.onFieldChange(e, 'domain') } } />
                    </div>
                </div>
                <div className="row">
                    <div className="col s12 text-center">
                        <RaisedButton label="Derived" secondary={true} onClick={ self.deriveEmail.bind(this) } />
                    </div>
                </div>
                <NotificationSystem ref="notificationSystem" />
            </div>
        )
    }

}