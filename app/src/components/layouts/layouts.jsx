import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

class AppLayout extends React.Component {

    render() {
        console.log('layout', this.props)
        return (
            <MuiThemeProvider>
                <div>
                    {this.props.children && React.cloneElement(this.props.children, {
                        app: this.props
                    })}
                </div>
            </MuiThemeProvider>
        );
    }
}

export default AppLayout;
