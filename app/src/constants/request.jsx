import axios from 'axios'
export const api = axios.create({
    baseURL: 'http://localhost:3000/v1/',
    timeout: 180000
});


export default class Request {
    constructor() {
    }

    get(url, params = {}, func = null, notification = null) {
        this.request_do('get', url, params, func)
    }

    post(url, params = {}, func = null, notification = null) {
        this.request_do('post', url, params, func, notification)
    }

    request_do(method, url, params, func, notification) {
        api({
            url: url,
            data: params,
            method: method
        }).then(function(response){
            if (func) {
                func(response)
            }
        }).catch(function(err){
        })
    }

}