import React from 'react';
import { Router, Route, IndexRoute, hashHistory } from 'react-router'
import Dashboard from '../components/dashboard'
import AppContainer from '../containers/app_containers'

export default class AppRouter extends React.Component {
    render() {
        return (
            <Router history={hashHistory}>
                <Route path="/"  component={ AppContainer } >
                    <IndexRoute name="root" component={ Dashboard } />
                </Route>
            </Router>
        )
    }
}