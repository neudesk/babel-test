import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import AppLayout from '../components/layouts/layouts'
import * as derive_email_actions from '../actions/derive_email_actions'

const mapStateToProps = (state) => {
    return {
        derived_email: state.Derives.email,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            derive_email_actions: bindActionCreators(derive_email_actions, dispatch)
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(AppLayout);