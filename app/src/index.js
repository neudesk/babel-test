import React from 'react';
import './default.css'
import ReactDOM from 'react-dom';
import App from './startup/App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
