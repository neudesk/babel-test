import Request from '../constants/request'
import queryString from 'query-string';
const request = new Request()

export function derivedEmail(params, notification = null) {
    return function(dispatch) {
        request.get('users/derive_email?'+queryString.stringify(params), {}, (response) => {
            const data = response.data;
            console.log('data', data)
            if (data) {
                if (data.error) {
                    console.log('error', data.error);
                    console.log('message', data.message);
                    notification.addNotification({
                        message: data.message,
                        level: 'error'
                    });
                } else {
                    dispatch({type: 'STORE_DERIVED_EMAIL', payload: data})
                }
            }
        }, notification)
    }
}