module ApiExceptions

  class SampleNotFound < StandardError; end
  class MissingRequiredParameters < StandardError; end
  class UnableToDeriveEmail < StandardError; end

end