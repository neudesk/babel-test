class String
  def initial
    self[0,1]
  end

  def unspaced
    self.gsub(/\s+/, '')
  end
end