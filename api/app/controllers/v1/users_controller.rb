class V1::UsersController < ApplicationController

  def derive_email
    render json: User.derive_email({ first_name: params[:first_name], last_name: params[:last_name], domain: params[:domain] })
  end

end
