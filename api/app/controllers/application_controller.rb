class ApplicationController < ActionController::API

  rescue_from ApiExceptions::MissingRequiredParameters, with: :handle_required_params
  rescue_from ApiExceptions::SampleNotFound, with: :sample_not_found
  rescue_from ApiExceptions::UnableToDeriveEmail, with: :unable_to_derive_email

  def handle_required_params(e)
    default_exception_response(e, 'firstname, lastname and domain are all required, please check your inputs.')
  end

  def sample_not_found(e)
    default_exception_response(e, 'Unable to find your domain.')
  end

  def unable_to_derive_email(e)
    default_exception_response(e, 'Unable to derive your email.')
  end

  private

  def default_exception_response(e, message = '')
    render json: { message: message, error: e.message, stacktrace: e.inspect }
  end

end
