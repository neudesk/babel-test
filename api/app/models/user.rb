require 'ext/string'

class User < ApplicationRecord

  validates :first_name, :last_name, :domain, presence: true

  class << self

    def validate_params(params)
      user = User.new(params)
      user.valid?
    end

    # email_patterns 1: first_name_last_name, 2: first_name_initial_last_name
    def get_formatted_email(sample, params)
      formatted_email = nil
      pattern_1 = [sample.first_name, sample.last_name].join('').unspaced.downcase
      pattern_2 = [sample.first_name.initial, sample.last_name].join('').unspaced.downcase
      user_name = sample.email.split('@')[0].downcase
      if user_name == pattern_1
        formatted_email = "#{[params[:first_name], params[:last_name]].join('')}@#{params[:domain]}"
      elsif user_name == pattern_2
        formatted_email = "#{[params[:first_name].initial, params[:last_name]].join('')}@#{params[:domain]}"
      end
      formatted_email.unspaced.downcase
    end

    def derive_email(params)
      raise ApiExceptions::MissingRequiredParameters unless validate_params(params)
      sample = User.find_by_domain(params[:domain].downcase)
      raise ApiExceptions::SampleNotFound unless sample.present?
      formatted_email = get_formatted_email(sample, params)
      raise ApiExceptions::UnableToDeriveEmail unless formatted_email.present?
      formatted_email
    end
  end

end
