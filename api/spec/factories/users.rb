require 'ext/string'

FactoryGirl.define do

  factory :user, class: User do
    first_name = Faker::Name.first_name.downcase.unspaced
    last_name = Faker::Name.last_name.downcase.unspaced
    domain = Faker::Internet.domain_name.downcase.unspaced

    factory :email_format_1 do
      first_name first_name
      last_name last_name
      domain domain
      email "#{first_name}#{last_name}@#{domain}"
    end

    factory :email_format_2 do
      first_name first_name
      last_name last_name
      domain domain
      email "#{first_name.initial}#{last_name}@#{domain}"
    end
  end

end
