require 'rails_helper'

RSpec.describe User, type: :model do

  context 'pattern 1' do
    let!(:sample) { FactoryGirl.create(:email_format_1) }
    let!(:params) { { first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, domain: sample.domain } }
    let(:email) { "#{params[:first_name].downcase.unspaced}#{params[:last_name].downcase.unspaced}@#{params[:domain]}" }

    it 'should able derived email' do
      expect(User.derive_email(params)).to eq email
    end

  end

  context 'pattern 2' do
    let!(:sample) { FactoryGirl.create(:email_format_2) }
    let!(:params) { { first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, domain: sample.domain } }
    let(:email) { "#{params[:first_name].downcase.unspaced.initial}#{params[:last_name].downcase.unspaced}@#{params[:domain]}" }

    it 'should able derived email' do
      expect(User.derive_email(params)).to eq email
    end

  end

end
