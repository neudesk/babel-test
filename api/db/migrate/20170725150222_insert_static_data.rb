class InsertStaticData < ActiveRecord::Migration[5.1]
  def up
    json = {
        "John Doe" => "jdoe@babbel.com",
        "Arun Jay" => "jayarun@linkedin.com",
        "David Stein" => "davidstein@google.com",
        "Mat Lee" => "matlee@google.com",
        "Marta Dahl" => "mdahl@babbel.com",
        "Vanessa Boom" => "vboom@babbel.com"
    }
    json.each do |k, v|
      name = k.split(' ')
      email = v.split('@')
      User.create(first_name: name[0].downcase, last_name: name[1].downcase, email: v.downcase, domain: email[1].downcase)
    end
  end

  def down
    User.all.delete_all
  end
end
