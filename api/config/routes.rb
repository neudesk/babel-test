Rails.application.routes.draw do

  namespace :v1 do
    resources :users do

      collection do
        get :derive_email
      end

    end
  end

end
